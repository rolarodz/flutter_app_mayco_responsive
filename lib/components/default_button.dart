import 'package:flutter/material.dart';

import 'package:flutter_app_mayco/config/constants.dart';
import 'package:flutter_app_mayco/config/size_config.dart';
import 'package:flutter_app_mayco/models/Responsive.dart';

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key key,
    this.text,
    this.press,
  }) : super(key: key);
  final String text;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [Responsive(
          mobile:
          _DefaultButtonMobile(text: text,press: press),
          desktop:
          _DefaultButtonDesktop(text: text,press: press),
        ),
        ]
    );
  }
}

class _DefaultButtonMobile extends StatelessWidget {
  //final TrackingScrollController scrollController;
  final String text;
  final Function press;

  const _DefaultButtonMobile({
    Key key,
    @required this.text,
    @required this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenHeight(56),//desde aqui (56)
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: kPrimaryColor,
        onPressed: press,
        child: Text(
          text,
          style: TextStyle(
            fontSize: getProportionateScreenWidth(18),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

class _DefaultButtonDesktop extends StatelessWidget {
  //final TrackingScrollController scrollController;
  final String text;
  final Function press;

  const _DefaultButtonDesktop({
    Key key,
    @required this.text,
    @required this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getProportionateScreenWidth(150),//double.infinity,
      height: getProportionateScreenHeight(50),//desde aqui (56)
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
        color: kPrimaryColor,
        onPressed: press,
        child: Text(
          text,
          style: TextStyle(
            fontSize: getProportionateScreenWidth(10),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}