import 'package:flutter/widgets.dart';
import 'package:flutter_app_mayco/screens/home/home_screen.dart';
import 'package:flutter_app_mayco/screens/initial/initial_screen.dart';
import 'package:flutter_app_mayco/screens/cart/cart_screen.dart';


final Map<String, WidgetBuilder> routes = {
  CartScreen.routeName: (context) => CartScreen(),
  InitialScreen.routeName: (context) => InitialScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
};