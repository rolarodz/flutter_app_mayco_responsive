import 'package:flutter/material.dart';

class Responsive extends StatelessWidget {
  final Widget mobile;
  final Widget tablet;
  final Widget desktop;

  const Responsive({
    Key key,
    @required this.mobile,
    this.tablet,
    @required this.desktop,
  }) : super(key: key);

  static bool isMobile(BuildContext context) =>
      MediaQuery.of(context).size.width < 800;

  static bool isTablet(BuildContext context) =>
      MediaQuery.of(context).size.width >= 800 &&
          MediaQuery.of(context).size.width < 1200;

  static bool isDesktop(BuildContext context) =>
      MediaQuery.of(context).size.width >= 1200;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        //var res = constraints.maxWidth;
        var res = MediaQuery.of(context).size.width;

        if (res >= 1200) {//(constraints.maxWidth >= 1200) {
          print('R-desktop');
          return desktop;
        } else if (res >= 800) {//(constraints.maxWidth >= 800) {
          print('R-tablet');
          return tablet ?? mobile;
        } else {
          print('R-mobile');
          return mobile;
        }
      },
    );
  }
}