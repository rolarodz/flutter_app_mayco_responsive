import 'package:flutter/material.dart';
import 'package:flutter_app_mayco/config/size_config.dart';

import 'categories.dart';
import 'discount_banner.dart';
import 'home_header.dart';
import 'popular_product.dart';
import 'special_offers.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              children: [
                SizedBox(height: getProportionateScreenHeight(20)),
                HomeHeader(),
                SizedBox(height: getProportionateScreenWidth(10)),
                DiscountBanner(),
                Categories(),
                SpecialOffers(),
                SizedBox(height: getProportionateScreenWidth(30)),
                PopularProducts(),
                SizedBox(height: getProportionateScreenWidth(30)),
              ],
            ),
            Positioned.fill(
              child:IgnorePointer(
                ignoring:true,
                child:Opacity(
                  opacity: 0.2,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                    child: Image.asset(
                      "images/data.png",
                      repeat: ImageRepeat.repeat,
                      alignment: Alignment.topCenter,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),

      ),
    );
  }
}