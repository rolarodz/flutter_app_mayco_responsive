import 'package:flutter/material.dart';
import 'package:flutter_app_mayco/config/size_config.dart';
import 'package:flutter_app_mayco/config/constants.dart';
import 'package:flutter_app_mayco/models/Responsive.dart';

class DiscountBanner extends StatelessWidget {
  const DiscountBanner({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Responsive(
          mobile:
          _DiscountBannerMobile(key: key),
          desktop:
          _DiscountBannerDesktop(key: key),
        )

    );
  }
}


class _DiscountBannerMobile extends StatelessWidget {
  const _DiscountBannerMobile({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 90,
      width: double.infinity,
      margin: EdgeInsets.all(getProportionateScreenWidth(20)),
      padding: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(60),
        vertical: getProportionateScreenWidth(45),
      ),
      decoration: BoxDecoration(
        color: kPrimaryColor,//Color(0xFF4A3298),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Text.rich(
        TextSpan(
          style: TextStyle(color: Colors.white),
          children: [
            //TextSpan(text: "No se fue,\n"),
            TextSpan(
              text: "¡Súper Ofertas MAYCO!",
              style: TextStyle(
                fontSize: getProportionateScreenWidth(24),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class _DiscountBannerDesktop extends StatelessWidget {
  const _DiscountBannerDesktop({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 90,
      width: double.infinity,
      margin: EdgeInsets.all(getProportionateScreenWidth(20)),
      padding: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(20),//60
        vertical: getProportionateScreenWidth(10),//45
      ),
      decoration: BoxDecoration(
        color: kPrimaryColor,//Color(0xFF4A3298),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Text.rich(
        TextSpan(
          style: TextStyle(color: Colors.white),
          children: [
            //TextSpan(text: "No se fue,\n"),
            TextSpan(
              text: "¡Súper Ofertas MAYCO!",
              style: TextStyle(
                fontSize: getProportionateScreenWidth(18),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
