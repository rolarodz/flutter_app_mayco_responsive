import 'package:flutter/material.dart';
import 'package:flutter_app_mayco/screens/cart/cart_screen.dart';
import 'package:flutter_app_mayco/config/size_config.dart';
import 'package:flutter_app_mayco/models/Responsive.dart';

import 'dart:developer';

import 'icon_btn_with_counter.dart';
import 'search_field.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({
    Key key,
  }) : super(key: key);

  void main() {
    log('log me', name: 'my.app.category');
  }

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    return Padding(
      padding:
      EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SearchField(),
          IconBtnWithCounter(
            svgSrc: "assets/icons/Cart Icon.svg",
            press: () => Navigator.pushNamed(context, CartScreen.routeName),
          ),
          IconBtnWithCounter(
            svgSrc: "assets/icons/Bell.svg",
            numOfitem: 3,
            press: () {},
          )/*,
          IconBtnWithCounter(
            svgSrc: "assets/icons/User.svg",
            press: () {},
          ),*/
        ],
      ),
    );
  }
}