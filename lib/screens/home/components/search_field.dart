import 'package:flutter/material.dart';

import 'package:flutter_app_mayco/config/constants.dart';
import 'package:flutter_app_mayco/config/size_config.dart';
import 'package:flutter_app_mayco/models/Responsive.dart';

class SearchField extends StatelessWidget {
  const SearchField({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      child: Responsive(
        mobile:
        _SearchFieldMobile(key: key),
        desktop:
        _SearchFieldDesktop(key: key),
      )

    );
  }
}

class _SearchFieldMobile extends StatelessWidget {
  const _SearchFieldMobile({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.screenWidth * 0.6,
      decoration: BoxDecoration(
        color: kSecondaryColor.withOpacity(0.1),
        borderRadius: BorderRadius.circular(15),
      ),
      child: TextField(
        onChanged: (value) => print(value),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20),
                vertical: getProportionateScreenWidth(9)),
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            hintText: "Buscar productos mobi",
            prefixIcon: Icon(Icons.search)),
      ),
    );
  }
}

class _SearchFieldDesktop extends StatelessWidget {
  const _SearchFieldDesktop({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child:
        Row( //Image.asset('assets/images/mayco.png'),
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset(
                  'assets/images/mayco.png',
                  width: 210,
                  height: 180,
                  fit:BoxFit.fill),
              Container(
                width: SizeConfig.screenWidth * 0.6,
                decoration: BoxDecoration(
                  color: kSecondaryColor.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: TextField(
                  onChanged: (value) => print(value),
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: getProportionateScreenWidth(15),
                          vertical: getProportionateScreenWidth(4)),
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      hintText: "Buscar productos desk",
                      prefixIcon: Icon(Icons.search)),
                ),
              )
            ]
        )
    );
  }
}
