import 'package:flutter/material.dart';
import 'package:flutter_app_mayco/components/coustom_bottom_nav_bar.dart';
import 'package:flutter_app_mayco/config/enums.dart';
import 'package:flutter_app_mayco/config/size_config.dart';
import 'package:flutter_app_mayco/models/Responsive.dart';

import 'components/body.dart';

class HomeScreen extends StatelessWidget {
  static String routeName = "/home";

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    SizeConfig().init(context);
    if (isDesktop){
      return Scaffold(
        body: Body(),//Body(),
      );
    }

    return Scaffold(
      body: Body(),//Body(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.home),
    );

  }
}