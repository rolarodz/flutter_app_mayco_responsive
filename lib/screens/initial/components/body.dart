import 'package:flutter/material.dart';
//import 'package:shop_app/constants.dart';
//import 'package:shop_app/screens/sign_in/sign_in_screen.dart';
import 'package:flutter_app_mayco/config/constants.dart';
import 'package:flutter_app_mayco/config/size_config.dart';
import 'package:flutter_app_mayco/components/default_button.dart';
import 'package:flutter_app_mayco/screens/initial/components/initial_content.dart';
import 'package:flutter_app_mayco/screens/home/home_screen.dart';


// This is the best practice
class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}


class _BodyState extends State<Body> {
  int currentPage = 0;
  List<Map<String, String>> initialData = [
    {
      "text": " ",
      "image": "assets/images/mayco.png"
    },
    {
      "text":
      "Desde tu mercado a tu tiendita",// \n
      "image": "assets/images/splash_2.png"
    },/*2
    {
      "text": "Desde tu mercado a tu tiendita.",
      "image": "assets/images/splash_3.png"
    },*/
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Stack(
          children:<Widget>[//
            Column(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: PageView.builder(
                    onPageChanged: (value) {
                      setState(() {
                        currentPage = value;
                      });
                    },
                    itemCount: initialData.length,
                    itemBuilder: (context, index) => InitialContent(
                      image: initialData[index]["image"],
                      text: initialData[index]['text'],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(20)),
                    child: Column(
                      children: <Widget>[
                        Spacer(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(
                            initialData.length,
                                (index) => buildDot(index: index),
                          ),
                        ),
                        Spacer(flex: 3),
                        DefaultButton(
                          text: "Ir a Comprar",
                          press: () {
                            Navigator.pushNamed(context, HomeScreen.routeName);
                          },
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned.fill(
              child:IgnorePointer(
                  ignoring:true,
                  child:Opacity(
                        opacity: 0.2,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                          child: Image.asset(
                            "images/data.png",
                            repeat: ImageRepeat.repeat,
                            alignment: Alignment.topCenter,
                          ),
                        ),
                  ),
              ),
            )
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}

/*
class _BodyState extends State<Body> {
  int currentPage = 0;
  List<Map<String, String>> initialData = [
    {
      "text": " ",
      "image": "assets/images/mayco.png"
    },
    {
      "text":
      "Desde tu mercado a tu tiendita",// \n
      "image": "assets/images/splash_2.png"
    },/*2
    {
      "text": "Desde tu mercado a tu tiendita.",
      "image": "assets/images/splash_3.png"
    },*/
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: PageView.builder(
                onPageChanged: (value) {
                  setState(() {
                    currentPage = value;
                  });
                },
                itemCount: initialData.length,
                itemBuilder: (context, index) => InitialContent(
                  image: initialData[index]["image"],
                  text: initialData[index]['text'],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Column(
                  children: <Widget>[
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        initialData.length,
                            (index) => buildDot(index: index),
                      ),
                    ),
                    Spacer(flex: 3),
                    DefaultButton(
                      text: "Ir a Comprar",
                      press: () {
                        Navigator.pushNamed(context, HomeScreen.routeName);
                      },
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}
*/