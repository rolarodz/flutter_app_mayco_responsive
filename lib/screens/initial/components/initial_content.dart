import 'package:flutter/material.dart';
import 'package:flutter_app_mayco/config/size_config.dart';
import 'package:flutter_app_mayco/config/constants.dart';

class InitialContent extends StatelessWidget {
  const InitialContent({
    Key key,
    this.text,
    this.image,
  }) : super(key: key);
  final String text, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Spacer(),
        /// First Title in initial screen
        Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(height: 10, fontSize: 20),
        ),
        Spacer(flex: 2),
        Image.asset(
          image,
          height: getProportionateScreenHeight(235),
          width: getProportionateScreenWidth(215),
        ),
      ],
    );
  }
}
