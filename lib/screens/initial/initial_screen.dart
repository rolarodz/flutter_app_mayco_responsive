import 'package:flutter/material.dart';
import 'package:flutter_app_mayco/config/size_config.dart';
import 'package:flutter_app_mayco/screens/initial/components/body.dart';

import 'package:flutter_app_mayco/config/constants.dart';


class InitialScreen extends StatelessWidget {
  static String routeName = "/initial";
  @override
  Widget build(BuildContext context) {
    // You have to call it on your starting screen
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
    );
  }
}
